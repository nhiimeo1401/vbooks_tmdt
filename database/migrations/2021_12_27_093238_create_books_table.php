<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->decimal('giaban');
            $table->integer('soluong');
            $table->unsignedBigInteger('cd_id');
            $table->string('tacgia');
            $table->string('nxb');
            $table->string('gioithieu');
            $table->string('hinhanh');
            $table->integer('taiban');
            $table->timestamps();

            $table->index('cd_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
