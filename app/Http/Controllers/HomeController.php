<?php

namespace App\Http\Controllers;

use App\Book;
use App\Category;
use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $xmlBookPath = 'xml/book.xml';
        $models = Book::all();
        $xml = response()->xml(['book'=>$models->toArray()]);
        Storage::put($xmlBookPath, $xml->getContent());
    }

    public function index()
    {
        $categories = Category::all()->sortBy('name');
        $books = Book::all()->sortBy('name');
        return view('home', compact('categories', 'books'));
    }

    public function category($cd_id)
    {
        $categories = Category::all()->sortBy('name');
        $books = Category::find($cd_id)->book;
        return view('category', compact('categories', 'books', 'cd_id'));
    }

    public function buy($book_id)
    {
        $book = Book::find($book_id);
        $sold = DB::table('order_details')->where('book_id',$book_id)->sum('quantity');
        $related_books = Book::where('category_id', $book->category_id)->take(8)->get();
        return view('buy', compact('book', 'sold', 'related_books'));
    }

    public function updateinfo() {
        $user = Auth::user();
        return view( 'updateinfo', compact( 'user' ) );
    }

    public function profile(){
        $histories = Auth::user()->order;
        return view('profile', compact('histories'));
    }

    public function doUpdateuser(Request $request, $id ) {
        $this->validate( request(), [
            'name' => 'required',
            'sdt' => 'required',
            'diachi' => 'required',
            'email'=> 'required'
        ] );
        $profile = Profile::where('user_id', $id)->first() ?? new Profile();
        $user = User::find( $id );
        $profile->sdt = $request->input( 'sdt' );
        $profile->user_id = $user->id;
        $profile->diachi = $request->input( 'diachi' );
        $profile->push();
        $user->name = $request->input( 'name' );
        $user->email = $request->input( 'email' );
        $user->push();

        Session::flash( 'success','Cập nhật thông tin cá nhân thành công ♥');
        return redirect( )->route('index');
    }

    public function all_product(){
        $books = Book::sortable()->paginate(12);
        $categories = Category::all();
        return view('product', compact('books', 'categories'));
    }

    public function filter(Request $request){
        if($request->has('range') && $request->has('categories')){
            $books = DB::table('books')->whereIn('category_id', $request->categories);
            $books = $books->whereBetween('giaban', $request->range);
            $books = $books->get();
        }
        else if($request->has('range')){
            $books = DB::table('books')->whereBetween('giaban', $request->range)->get();
        }
        else{
            $books = DB::table('books')->whereIn('category_id', $request->categories);
        }
        $html = '';
        foreach ($books as $item){
            $html .= sprintf("<article class=\"short-item\">
                        <div class=\"short-item__all\">
                            <a class=\"short-item__image-bg\" href=\"%s\">
                                <img class=\"short-item__image\" src=\"%s\" alt=\"\">
                            </a>
                            <h4 class=\"short-item__title\">
                                <a class=\"short-item__link\" href=\"%s\">%s</a>
                            </h4>
                            <span class=\"short-item__price\">%s VNĐ</span>
                        </div>
                    </article>", route('buy', $item->id), asset('/images/' . $item->hinhanh), route('buy', $item->id), $item->name, number_format($item->giaban));
        }

        return $html;
    }

}
