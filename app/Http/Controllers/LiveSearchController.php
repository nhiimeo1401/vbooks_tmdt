<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class LiveSearchController extends Controller
{
    //
    public function doSearch(Request $request){
        $key = $request->get('key');
        $result = DB::table('books')->where('name', 'like', '%' . $key . '%' )
            ->orderBy('id', 'desc')
            ->take(5)
            ->get();
        return json_encode($result);
    }

    public function xmlsearch($query){
        $xmlString = Storage::get('xml/book.xml');
        $xmlObject = simplexml_load_string($xmlString);

        $json = json_encode($xmlObject);
        $phpArray = json_decode($json, true);
        $result = '';
        foreach ($phpArray['book'] as $book){
            if(str_contains($book['name'], $query)){
                $result .= '<li class="hide-nav__item"><a href="' . route('buy', $book['id']) . '" class="hide-nav__link">' . $book['name'] . '</a>';
            }
        }

        return $result;
    }
}
