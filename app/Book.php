<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Book extends Model
{
    use Sortable;
    //
    protected $fillable = ['id','name','giaban','soluong','category_id','tacgia',
        'nxb','gioithieu','hinhanh','taiban'];

    public $sortable = ['name',
        'id',
        'category_id',
        'giaban'];

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function order_details(){
        return $this->belongsTo(Order::class);
    }
}
