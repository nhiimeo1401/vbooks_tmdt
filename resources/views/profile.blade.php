@extends('layouts.layout')
@section('content')
    <?php $user = Auth::user(); ?>
    <?php $default_avt = asset("assets/frontend/images/default_avt.jpg") ?>
    <div class="profile-page wrapper" style="margin: auto;">
        <div class="profile-page__rows">
            <!-- BEGIN LEFT COLUMN -->
            <div class="profile-page__left">
                <span class="tabs-nav__link">Thông tin cá nhân</span>
                <div class="profile-page__avatar">
                    <img src="{{$user->profile->avatar ?? $default_avt}}" alt="avatar">
                </div>
                <div class="profile-page__info">
                    <p class="newsletter-block__text"><strong>{{$user->name}}</strong></p>
                    <p class="newsletter-block__text">Địa chỉ: {{$user->profile->diachi ?? 'Chưa cập nhật'}}</p>
                    <p class="newsletter-block__text">Số điện thoại: {{$user->profile->sdt ?? 'Chưa cập nhật'}}</p>
                    <p class="newsletter-block__text">Địa chỉ email: {{$user->email}}</p>
                </div>
            </div>
            <div class="profile-page__left">
                <span class="tabs-nav__link">Lịch sử mua hàng ({{count($histories)}}): </span>
                <div class="orders js-faq">
                    @foreach($histories->sortByDesc('id')->take(3) as $item)
                        <div class="order js-faq-item">
                            <div class="order__top">
                                <a class="order__button js-faq-button" href="javascript:void(0);">
                                    <div class="order-header">
                                        <div class="order-header__col">
                                            <span class="order-header__number">Đơn hàng số: {{$item->id}}</span>
                                        </div>
                                        <div class="order-header__col">
                                            <span class="order-header__date">{{$item->created_at}}</span>
                                        </div>
                                        <div class="order-header__col">
                                            @if($item->status == 'Đang chờ')
                                                <div class="order-header__status order-status orange">
                                                    <div class="order-status__cols">
                                                        <div class="order-status__col">{{$item->status}}</div>
                                                        <div class="order-status__col">
                                                            <img class="order-status__icon"
                                                                 data-lazy="{{asset('assets/user/img/svg/order-icon_1.svg')}}"
                                                                 alt="" style="width:24px; height:19px;">
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="order-header__status order-status green">
                                                    <div class="order-status__cols">
                                                        <div class="order-status__col">{{$item->status}}</div>
                                                        <div class="order-status__col">
                                                            <img class="order-status__icon"
                                                                 data-lazy="{{asset('assets/user/img/svg/order-icon_2.svg')}}"
                                                                 alt="" style="width:24px; height:19px;">
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="order__hide js-faq-hide">
                                <div class="order__content">
                                    <?php $total = 0; ?>
                                    <div class="order-table">
                                        <?php $details = \App\OrderDetails::where('order_id', $item->id)->get(); ?>
                                        @foreach($details as $detail)
                                            <?php $book = \App\Book::find($detail->book_id); ?>
                                            <article class="order-table__item">
                                                <div class="order-table__cols">
                                                    <div class="order-table__col">
                                                        <a class="order-table__image-link"
                                                           href="{{route('buy', $book->id)}}">
                                                            <img class="order-table__image"
                                                                 src="{{asset('/images/' . $book->hinhanh)}}"
                                                                 alt="">
                                                        </a>
                                                    </div>
                                                    <div class="order-table__col">
                                                        <h4 class="order-table__title">
                                                            <a class="order-table__link"
                                                               href="{{route('buy', $book->id)}}">{{$book->name}}</a>
                                                        </h4>
                                                        <span
                                                            class="order-table__text">x{{$detail->quantity}}</span>
                                                    </div>
                                                    <div class="order-table__col">
                                                            <span
                                                                class="order-table__price">{{number_format($book->giaban) . ' VNĐ'}}</span>
                                                    </div>
                                                </div>
                                            </article>
                                            <?php $total += $book->giaban * $detail->quantity; ?>
                                        @endforeach
                                    </div>
                                    <div class="order-total">
                                        <div class="order-total__item">
                                            <div class="order-total__col">
                                                <span class="order-total__title">Tổng tiền:</span>
                                            </div>
                                            <div class="order-total__col">
                                                <div
                                                    class="order-total__price">{{number_format($total) . ' VNĐ'}}</div>
                                            </div>
                                        </div>
                                        <div class="order-total__item">
                                            <div class="order-total__col">
                                                <span class="order-total__title">Địa chỉ nhận hàng:</span>
                                            </div>
                                            <div class="order-total__col">
                                                @if(($profile = auth()->user()->profile) != null)
                                                    <div class="order-total__text">{{$profile->diachi}}</div>
                                                @else
                                                    <div class="order-total__text"></div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    @if(count($histories) > 3)
                        <a href="#" class="more__button">Xem thêm</a>
                    @endif
                </div>
            </div>
        </div>
        <!-- LEFT COLUMN END -->
    </div>
@endsection
